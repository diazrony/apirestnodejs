//============================
// Port
//============================
process.env.PORT = process.env.PORT || 3000
//============================
// Enviroment
//============================
process.env.NODE_ENV = process.env.NODE_ENV || 'dev'
//============================
// Expiration token
//============================
process.env.EXPIRATION_TOKEN = '48h'
//============================
// Seed autentication
//============================
process.env.SEED = process.env.SEED || 'this-is-secret-dev'
//============================
// Google Client ID
//============================
process.env.CLIENT_ID = process.env.CLIENT_ID || '334010518200-9nfhegdf5u0suhr43u8s5sbh5uns44ib.apps.googleusercontent.com'
//============================
// DataBase
//============================
let urlDataBase;

if ( process.env.NODE_ENV === 'dev' ) {
    urlDataBase = 'mongodb://localhost:27017/cafe'
}else{
    urlDataBase = process.env.MONGO_URI
}

process.env.URLDB = urlDataBase