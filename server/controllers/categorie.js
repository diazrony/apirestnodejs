const express = require('express');
let {checkToken, checkAdminRole} = require('../middlewares/authentication');
let Categorie = require('../models/categorie')
const app = express();

//View all categories
app.get('/categories',checkToken,(req , res) => {
    Categorie.find({})
    .sort('description')
    .populate('user', 'name email')
    .exec((err , categories) => {
        if(err){
            return res.status(500).json({
                ok: false,
                err
            })
        }
        if(!categories){
            return res.status(400).json({
                ok: false,
                err
            })
        }
        res.json({
            ok:true,
            categories
        })
    })
})
//View all categories
app.get('/categories/:id',[checkToken], (req , res) => {
    let id = req.params.id

    Categorie.findById(id, (err, categorieDB) => {
        if(err) {
            return res.status(500).json({
                ok: false,
                err
            })
        }
        if(!categorieDB){
            return res.status(400).json({
                ok: false,
                err: {
                    err,
                    message: `Categorie with id ${id} doesn´t exist`
                }
            })
        }
        res.json({
            ok:true,
            categorie: categorieDB
        })
    })


})
//Created new Categories
app.post('/categories', checkToken ,(req, res) => {
    let body = req.body
    let categories = new Categorie({
        description: body.description,
        user: req.user._id
    })

    categories.save( (err , categorieDB) => {
        if(err){
            return res.status(500).json({
                ok: false,
                err
            })
        }
        if(!categorieDB){
            return res.status(400).json({
                ok: false,
                err
            })
        }
        res.json({
            ok:true,
            categorie: categorieDB
        })
    } )

})
//Update Categories Name
app.put('/categories/:id', (req , res) => {
    let id = req.params.id
    let body = req.body

    let desCategorie = {
        description: body.description
    }

    Categorie.findByIdAndUpdate(id, desCategorie,{new: true, runValidators: true}, (err, categorieDB) => {
        if(err){
            return res.status(500).json({
                ok: false,
                err
            })
        }
        if(!categorieDB){
            return res.status(400).json({
                ok: false,
                err
            })
        }
        res.json({
            ok:true,
            categorie: categorieDB
        })
    })
})
//Delete Categories
app.delete('/categories/:id',[checkToken, checkAdminRole], (req , res) => {
    let id = req.params.id
    Categorie.findByIdAndRemove(id, (err ,categorieDB ) => {
        if(err){
            return res.status(500).json({
                ok: false,
                err
            })
        }
        if(!categorieDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'id doesn´t exist'
                }
            })
        }
        res.json({
            ok:false,
            message: 'Delete Categorie'
        })
    })
})
module.exports = app