const express = require('express');
const fs = require('fs');
const path = require('path');
const app  = express();
const {checkTokenImagen} = require('../middlewares/authentication');

app.get('/image/:type/:img', checkTokenImagen ,( req , res) => {
    let type = req.params.type
    let img = req.params.img

    //let pathImg = `./uploads/${type}/${ img }`
    let pathImage = path.resolve(__dirname, `../../uploads/${ type }/${ img }`) 
    let pathNoImage = path.resolve(__dirname, '../assets/image-default.png')

    if (fs.existsSync(pathImage)) {
        res.sendFile(pathImage)
    }else {
        res.sendFile(pathNoImage)
    }
    
})

module.exports = app;