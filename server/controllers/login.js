const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {OAuth2Client} = require('google-auth-library');
const client = new OAuth2Client(process.env.CLIENT_ID);
const User = require('../models/user');
const app = express();
//POST
app.post('/login', async ( req , res ) => {
    let body = req.body
    let user = await User.findOne({email: body.email})
    if( user ) {
        let comparation = await bcrypt.compareSync(body.password, user.password)
        if( !comparation ){
            res.status(400).json({
                ok: false,
                message: "user or (password) incorrect"
            })
        }
        let token = await jwt.sign({user}, process.env.SEED ,{expiresIn: process.env.EXPIRATION_TOKEN})
        res.json({
            ok: true,
            user,
            token
        })
        
    }else{
        res.status(400).json({
            ok: false,
            message: "(user) or password incorrect"
        })
    }
})
//============================
// Config Google
//============================
async function verify( token ) {
    const ticket = await client.verifyIdToken({
        idToken: token,
        audience: process.env.CLIENT_ID,  // Specify the CLIENT_ID of the app that accesses the backend
        // Or, if multiple clients access the backend:
        //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
    });
    const payload = ticket.getPayload();
    return {
        name: payload.name,
        email: payload.email,
        img: payload.picture,
        google: true
    }
}  
//POST
app.post('/google', async ( req , res ) => {
    let token = req.body.idtoken;
    let userGoogle = await verify(token).catch(e => {
        return res.status(403).json({ok:false,err:e})
    })

    User.findOne({email: userGoogle.email}, ( err , userDB ) => {
        if (err) {
            return res.status(500).json({
                ok:false,
                err
            })
        }
        if (userDB) {
            if( userDB.google === false ) {
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'Use your authentication normal'
                    }
                })
            }else{
                let token = jwt.sign({user:userDB}, process.env.SEED ,{expiresIn: process.env.EXPIRATION_TOKEN});
                return res.json({
                    ok:true,
                    user: userDB,
                    token
                })
            }
        }else{
            //if the user doesn´t exist in DataBase is the new user
            let user = new User();
            user.name = userGoogle.name
            user.email = userGoogle.email
            user.img = userGoogle.img
            user.google = true
            user.password = ':)'

            user.save((err , userDB) => {
                if (err) {
                    return res.status(500).json({
                        ok:false,
                        err
                    })
                }
                let token = jwt.sign({user:userDB}, process.env.SEED ,{expiresIn: process.env.EXPIRATION_TOKEN});
                return res.json({
                    ok: true,
                    user:userDB,
                    token
                })
            })
        }
    })
    
})
module.exports = app;
