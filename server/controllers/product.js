const express = require('express');
let {checkToken, checkAdminRole} = require('../middlewares/authentication');
let Product = require('../models/product')
const app = express();

//Get products
app.get('/product', checkToken,(req, res) => {
    let start = req.query.start || 0
    start = Number(start)
    Product.find({available: true})
        .skip(start)
        .limit(5)
        .populate('user', 'name email')
        .populate('categorie')
        .exec((err , Products) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    err
                })
            }

            res.json({
                ok:true,
                products: Products
            })
        })
})

//Get product
app.get('/product/:id', checkToken ,(req, res) => {
    let id = req.params.id

    Product.findById(id)
        .populate('user', 'name email')
        .populate('categorie')
        .exec((err, productDB) => {

        if (!productDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: `id ${id} no exist`
                }
            })
        }
        if (err) {
            return res.status(500).json({
                ok: false,
                err,
                message: `id ${id} no exist`
            })
        }
        res.json({
            ok:true,
            product: productDB
        })
    })

})
//Get Search product
app.get('/product/search/:word', checkToken ,(req, res) => {
    let word = req.params.word
    let regexp = new RegExp(word,'i')
    Product.find({name: regexp})
        .populate('categorie', 'name')
        .exec((err , products) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    err,
                    message: `id ${id} no exist`
                })
            }
            res.json({
                ok: true,
                products
            })
        })
})
//Post created product
app.post('/product', checkToken ,(req, res) => {
    let body = req.body
    let product = new Product({
        user: req.user._id,
        name: body.name,
        unitPrice: body.unitPrice,
        description: body.description,
        available: body.available,
        categorie: body.categorie
    })
    product.save( (err, productDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            })
        }
        res.status(201).json({
            ok: true,
            product: productDB
        })
    })
})

//Put update product
app.put('/product/:id', [checkToken, checkAdminRole] ,(req, res) => {
    let id = req.params.id
    let body = req.body
    let product = Product.findById(id, (err, productDB) => {
        if(!productDB){
            return res.status(400).json({
                ok: false,
                err
            })
        }
        if(err){
            return res.status(500).json({
                ok: false,
                err
            })
        }
        productDB.name = body.name
        productDB.unitPrice = body.unitPrice
        productDB.description = body.description
        productDB.available = body.available
        productDB.categorie = body.categorie
        productDB.user = body.user

        productDB.save((err, productSave) => {
            if(err){
                return res.status(500).json({
                    ok: false,
                    err
                })
            }
            res.json({
                ok: true,
                product: productSave
            })
        })

    })
})

//Delete product
app.delete('/product/:id', [checkToken, checkAdminRole], (req, res) => {
    let id = req.params.id
    Product.findById(id, (err, productDB) => {
        if(!productDB){
            return res.status(400).json({
                ok: false,
                err: {
                    message: `id ${id} doesn´t exist`
                }
            })
        }
        if(err){
            return res.status(500).json({
                ok: false,
                err
            })
        }
        productDB.available = false
        productDB.save((err, productDelete) => {
            res.json({
                ok: true,
                productDelete
            })
        })

    })
})

module.exports = app