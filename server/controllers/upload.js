const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();
const User = require('./../models/user')
const Product = require('../models/product')
const fs = require('fs');
const path = require('path');

app.use(fileUpload())

app.put('/upload/:type/:id', ( req , res ) => {
    let type = req.params.type
    let id = req.params.id

    if (!req.files) {
        return res.status(400).json({ok:false, message: 'No files were uploaded'})
    }
    //Valid type
    let typeValid = [ 'product', 'user' ]
    if ( typeValid.indexOf( type ) < 0 ) {
        return res.status(400).json({
            ok: false,
            message: `The type valid are ${typeValid.join(', ')}`,
            type
        })
    }


    let file = req.files.file

    //Extension Valid
    let extensionValid = ['png', 'jpg' , 'gif' , 'jpeg', 'PNG']
    let nameCut = file.name.split('.')
    let extension = nameCut[1]
    if (extensionValid.indexOf( extension ) < 0 ) {
        return res.status(400).json({
            ok: false,
            message: `The extension valid are ${extensionValid.join(', ')}`,
            extension
        })
    }
    //Change File name
    let fileName = `${id}-${new Date().getMilliseconds()}.${extension}`

    file.mv(`uploads/${type}/${fileName}`, (err) => {
        if (err) {
            return res.status(500).json({ok:false, err})
        }
        switch (type) {
            case 'product':
                productImage(id, res, fileName)
                break;
            case 'user':
                userImage(id, res, fileName)
                break;
            default:
                res.status(400).json({
                    ok:false,
                    message: 'the type path is incorrect'
                })
                break;
        }
        

    })
})

const userImage = (id , res, fileName) => {
    User.findById(id, ( err, userDB ) => {
        if (err) {
            deleteFile(fileName, 'user')
            return res.status(500).json({
                ok: false,
                err
            })
        }
        if (!userDB) {
            deleteFile(fileName, 'user')
            return res.status(400).json({
                ok: false,
                message: 'user no find',
                err
            })
        }
        //Delete File
        deleteFile(userDB.image, 'user')
        //New Name Image
        userDB.image = fileName
        userDB.save( (err , userDBSave) => {
            res.json({
                ok: true,
                user: userDBSave,
                img: fileName
            })
        })
    })
}

const productImage = (id , res, fileName) => {
    Product.findById(id, ( err, productDB ) => {
        if (err) {
            deleteFile(fileName, 'user')
            return res.status(500).json({
                ok: false,
                err
            })
        }
        if (!productDB) {
            deleteFile(fileName, 'user')
            return res.status(400).json({
                ok: false,
                message: 'user no find',
                err
            })
        }
        //Delete File
        deleteFile(productDB.image, 'user')
        //New Name Image
        productDB.image = fileName
        productDB.save( (err , productDBSave) => {
            res.json({
                ok: true,
                product: productDBSave,
                img: fileName
            })
        })
    })
}

const deleteFile = (nameImage, typeUrlPath) => {
    let pathImage = path.resolve(__dirname, `../../uploads/${ typeUrlPath }/${ nameImage }`) 

    if ( fs.existsSync(pathImage)) {
        fs.unlinkSync(pathImage)
    }
}

module.exports = app;
