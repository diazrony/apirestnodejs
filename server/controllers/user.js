const express = require('express');
const bcrypt = require('bcrypt');
const underscore = require('underscore');
const User = require('../models/user');
const { checkToken, checkAdminRole } = require('../middlewares/authentication')
const app = express();


app.get('/user',checkToken ,(req, res) => {
    let start = req.query.start || 0;
    let limit = req.query.limit || 5;
    start = Number(start)
    limit = Number(limit)
    User.find({state: true}, 'name email role state google image')
    .skip(start)
    .limit(limit)
    .exec((err, users) => {
        if (err) {
            res.status(400).json({
                ok: false,
                err
            })
        }
        User.countDocuments({state: true}, (err, count) => {
            res.json({
                ok: true,
                count,
                users
            })
        })
    })
})
app.post('/user',[checkToken, checkAdminRole], ( req, res ) => {
    let body = req.body

    let user = new User(
        {
            name: body.name,
            email: body.email,
            password: bcrypt.hashSync(body.password, 10),
            role: body.role
        }
    );
    user.save((err, userDb) => {
        if (err) {
            res.status(400).json({
                ok: false,
                err
            })
        }
        //userDb.password = null
        res.json({
            ok: true,
            user: userDb
        })
    })
})
app.put('/user/:id',[checkToken, checkAdminRole], ( req, res ) => {
    let id = req.params.id
    let body = underscore.pick(req.body, ['name','email','image','role','state']);
    User.findByIdAndUpdate( id , body , {new: true, runValidators: true} ,(err , userDb) => {
        if (err) {
            res.status(400).json({
                ok: false,
                err
            })
        }
        res.json({
            ok: true,
            user: userDb
        })
    })
})
app.delete('/user/:id',[checkToken, checkAdminRole], ( req, res ) => {
    let id = req.params.id
    let changeState = { state: false }
    
    // User.findByIdAndRemove(id,(err, userDelete) => {
    //     if (err) {
    //         res.status(400).json({
    //             ok: false,
    //             err
    //         })
    //     }
    //     if ( !userDelete ){
    //         res.status(400).json({
    //             ok: false,
    //             user: 'User no finded'
    //         })
    //     }
    //     res.json({
    //         ok: true,
    //         userDelete
    //     })
    // })
    User.findByIdAndUpdate( id , changeState , { new: true } ,( err, userDelete ) => {
        if (err) {
            res.status(400).json({
                ok: false,
                err
            })
        }
        if ( !userDelete ){
            res.status(400).json({
                ok: false,
                user: 'User no finded'
            })
        }
        res.json({
            ok: true,
            userDelete
        })
    })
});

module.exports = app;