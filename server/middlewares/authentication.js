const jwt = require('jsonwebtoken');
//============================
// Check Token
//============================
let checkToken =  ( req, res, next ) => {
    let token = req.get('Authorization')
    jwt.verify(token,process.env.SEED, ( err , payload ) => {
        if ( err ) {
            return res.status(401).json({
                ok: false,
                err
            })
        }
        req.user = payload.user
        next()
    })
}
//============================
// Check AdminRole
//============================
let checkAdminRole = ( req , res , next) => {
    let user = req.user
    if( user.role === 'ADMIN_ROLE') {
        next()
    }else{
        res.json({
            message: 'user no valid your role no is admin'
        })
    }
}
//============================
// Check Imagen
//============================
let checkTokenImagen = ( req , res , next ) => {
    let token = req.query.token

    jwt.verify(token,process.env.SEED, ( err , payload ) => {
        if ( err ) {
            return res.status(401).json({
                ok: false,
                err
            })
        }
        req.user = payload.user
        next()
    })   
}

module.exports = {
    checkToken,
    checkAdminRole,
    checkTokenImagen
}