const mongoose = require('mongoose')
const Schema = mongoose.Schema;

let categorieSchema = new Schema({
    description: { type: String, unique: true, required: [true, 'The description is required'] },
    user: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        }
});

module.exports = mongoose.model('Categorie', categorieSchema)