var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var productSchema = new Schema({
    name: { type: String, required: [true, 'The name is necesary'] },
    unitPrice: { type: Number, required: [true, 'the unit price is necesary'] },
    description: { type: String, required: false },
    available: { type: Boolean, required: true, default: true },
    categorie: { type: Schema.Types.ObjectId, ref: 'Categorie',required: true },
    user: { type: Schema.Types.ObjectId, ref: 'user' },
    image: {type: String, required: false}
});


module.exports = mongoose.model('Product', productSchema);