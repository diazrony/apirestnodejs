const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
let rolesValid = {
    values: ['ADMIN_ROLE','USER_ROLE'],
    message: '{VALUE}, no is role valid'
}
let Shema = mongoose.Schema;

let userShema = new Shema({
    name:{
        type: String,
        required: [true, 'Name is necesary']
    },
    email: {
        type: String,
        unique: true,
        index: true,
        required: [true, 'Email is necesary'],
        
    },
    password: {
        type: String,
        required: [true, 'Password is necesary']
    },
    image: {
        type: String,
        required: false
    },
    role: {
        type: String,
        default: 'USER_ROLE',
        enum: rolesValid
    },
    state: {
        type: Boolean,
        default: true
    },
    google: {
        type: Boolean,
        default:false
    }
},{timestamps: true} );

userShema.methods.toJSON = function () {
    let usuario = this;
    let usuarioObject = usuario.toObject();
    delete usuarioObject.password;
    return usuarioObject;
}
userShema.plugin( uniqueValidator, {message: '{PATH}  It must be unique'} )

module.exports = mongoose.model('user', userShema)